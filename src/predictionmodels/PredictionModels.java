package predictionmodels;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.ADTree;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.supervised.instance.Resample;

public class PredictionModels {
	private static String dir = "output2/";

	public static void main(String[] args) throws Exception {
		List<File> allFiles = getAllfiles("dataset/", "arff");

		ArrayList<String> projNames = new ArrayList<String>();

		for (File a : allFiles) {
			projNames.add(a.getName());
		}

		for (String proj : projNames) {

			Instances TrainSet = new DataSource(dir + proj + "_Train.arff").getDataSet();
			TrainSet.setClassIndex(TrainSet.numAttributes() - 1);
			//remove the sentiment feature
			TrainSet.deleteAttributeAt(15);
			Instances TestSet = new DataSource(dir + proj + "_Test.arff").getDataSet();
			TestSet.setClassIndex(TestSet.numAttributes() - 1);
			//remove the sentiment feature
			TestSet.deleteAttributeAt(15);
			 
			// SMO cla1 = new SMO();
			// IBk cla1=new IBk(5);
			RandomForest cla1 = new RandomForest();
			// NaiveBayes cla1 = new NaiveBayes();
			cla1.buildClassifier(TrainSet);

			Evaluation eval = new Evaluation(TrainSet);
			eval.evaluateModel(cla1, TestSet);

			 
			System.out.println(proj + "\t" + eval.precision(1) + "\t" + eval.recall(1) + "\t" + eval.fMeasure(1) + "\t"
					+ eval.areaUnderROC(1) + "\t" + (1 - eval.errorRate()));
		}

	}

	public static List<File> getAllfiles(String path, String extension) {
		ArrayList<File> ret = new ArrayList<File>();
		try {
			Files.walk(Paths.get(path)).forEach(filePath -> {
				if (Files.isRegularFile(filePath))
					if (filePath.toString().toLowerCase().endsWith("." + extension))
						ret.add(filePath.toFile());
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
